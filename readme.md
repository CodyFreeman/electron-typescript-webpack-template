# Electron TypeScript Webpack Template

**NB! This is not ready for production!**

This is a starter template for developing electron apps with TypeScript and Webpack 4.

## Setup

Clone this repository and run `npm install` in the project folder.

## Usage

Run `npm start` to build and launch the application.

Run `npm run transpile:development` or `npm run transpile:production` to transpile your application with Webpack.

**NB! Currently no Electron production build capabilities**

### Analyze Bundle Size

To analyze bundle size for main and renderer script, run `npm run analyze:main` or `npm run analyze:renderer`.

## HTML Template

The file `src/index.html` is used as a template for webpack and will automatically include `src/renderer.ts` during build.

## Webpack Configuration

See documentation in `webpack/readme.md` when Webpack configuration has stabilized.
