import { BrowserWindow, app, ipcMain } from "electron";

function spawn() {
  const win = new BrowserWindow({
    height: 600,
    width: 800,
    webPreferences: { nodeIntegration: true },
  });

  win.loadFile(`./index.html`);
  win.webContents.openDevTools();
}

app.on("ready", () => {
  spawn();
});

app.on("window-all-closed", () => {
  app.quit();
});

ipcMain.on("ping", (event) => {
  event.reply("pong");
});
