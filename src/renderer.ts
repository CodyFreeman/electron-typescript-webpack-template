import { ipcRenderer } from "electron";

const button = document.getElementById("ping");
if (!button) {
  throw new Error("no button");
}

let counter = 0;
button.addEventListener("click", () => {
  ipcRenderer.send("ping");
});

ipcRenderer.on("pong", () => {
  counter++;
  button.innerText = `Ping resolved ${counter} time${counter > 1 ? "" : "s"}`;
});
