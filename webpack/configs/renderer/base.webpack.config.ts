import { Configuration, ProgressPlugin } from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { cwd } from "process";
import { EnvParsed } from "../../utils/envInterfaces";
export default function (env: EnvParsed): Configuration {
  return {
    mode: "production",
    output: {
      path: `${cwd()}/dist`,
      filename: "renderer.js",
    },
    entry: `${cwd()}/src/renderer.ts`,
    target: "electron-preload",
    resolve: { extensions: [".ts", "tsx", "json"] },
    plugins: [
      new HtmlWebpackPlugin({ template: `${cwd()}/src/index.html` }),
      new ProgressPlugin(),
    ],
    module: {
      rules: [
        {
          test: /\.ts$/,
          loader: "ts-loader",
          options: { configFile: `${cwd()}/tsconfig.json` },
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.scss$/,
          use: ["style-loader", "css-loader", "sass-loader"],
        },
        {
          test: /\.png|jpe?g$/,
          use: [
            {
              // Data url loader
              loader: "url-loader",
              options: {
                // Only inline if smaller than 5kb
                limit: 5000,
              },
            },
          ],
        },
      ],
    },
  };
}
