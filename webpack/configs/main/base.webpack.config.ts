import { Configuration, ProgressPlugin } from "webpack";
import { cwd } from "process";
import { EnvParsed } from "../../utils/envInterfaces";
export default function (env: EnvParsed): Configuration {
  return {
    mode: "production",
    output: {
      path: `${cwd()}/dist`,
      filename: "main.js",
    },
    target: "electron-main",
    entry: `${cwd()}/src/main.ts`,
    plugins: [new ProgressPlugin()],
    module: {
      rules: [
        {
          test: /\.ts$/,
          loader: "ts-loader",
          options: { configFile: `${cwd()}/tsconfig.json` },
        },
      ],
    },
  };
}
