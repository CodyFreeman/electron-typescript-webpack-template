import { Configuration } from "webpack";
import { EnvParsed } from "../../utils/envInterfaces";
export default function (env: EnvParsed): Configuration {
  return {
    mode: "production",
  };
}
