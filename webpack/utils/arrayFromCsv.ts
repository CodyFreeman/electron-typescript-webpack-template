export function arrayFromCsv(presets: string): string[] {
  if (!presets || !presets.length) {
    return [];
  }
  if (presets.search(",") === -1) {
    return [presets];
  }
  return presets.split(",");
}
