import { Configuration } from "webpack";
export interface EnvInput {
  mode: Configuration["mode"];
  /** Comma separated names of preset names */
  presets: string;
  script: "main" | "renderer";
}
export interface EnvParsed {
  mode: Configuration["mode"];
  presets: string[];
  script: "main" | "renderer";
}
