import CompressionWebpackPlugin from "compression-webpack-plugin";
import { Configuration } from "webpack";
export default function (): Configuration {
  return {
    plugins: [new CompressionWebpackPlugin()],
  };
}
