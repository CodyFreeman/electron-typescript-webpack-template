import { Configuration } from "webpack";
import { merge } from "webpack-merge";

export class PresetManager {
  public loadPresets(env: any): Configuration {
    const { presets } = env;
    if (!presets || !presets.length) {
      return {};
    }
    const mergedPresets = [].concat(...[presets]);
    const configs = mergedPresets.map((name) => {
      return require(`./${name}`).default(env);
    });
    return merge({}, ...configs);
  }
}
