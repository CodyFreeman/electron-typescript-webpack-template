import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
import { Configuration } from "webpack";
export default function (): Configuration {
  return {
    plugins: [new BundleAnalyzerPlugin()],
  };
}
