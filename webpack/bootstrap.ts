import { Configuration } from "webpack";
import { merge } from "webpack-merge";
import mainBaseConfig from "./configs/main/base.webpack.config";
import mainDevConfig from "./configs/main/dev.webpack.config";
import mainProdConfig from "./configs/main/prod.webpack.config";
import rendererBaseConfig from "./configs/renderer/base.webpack.config";
import rendererDevConfig from "./configs/renderer/dev.webpack.config";
import rendererProdConfig from "./configs/renderer/prod.webpack.config";

import { PresetManager } from "./presets/PresetManager";
import { EnvInput, EnvParsed } from "./utils/envInterfaces";
import { arrayFromCsv } from "./utils/arrayFromCsv";

export default function (
  env: EnvInput = { mode: "production", presets: "", script: "renderer" }
): Configuration[] {
  const parsedEnv: EnvParsed = Object.assign({}, env, {
    presets: arrayFromCsv(env.presets),
  });

  const mainConfig = createMainConfig(parsedEnv);
  const rendererConfig = createRenderConfig(parsedEnv);

  if (env.script === "main") {
    return [mainConfig];
  }
  if (env.script === "renderer") {
    return [rendererConfig];
  }
  return [mainConfig, rendererConfig];
}

function createMainConfig(env: EnvParsed) {
  return merge(
    {},
    mainBaseConfig(env),
    env.mode === "development" ? mainDevConfig(env) : mainProdConfig(env),
    new PresetManager().loadPresets(env)
  );
}

function createRenderConfig(env: EnvParsed) {
  return merge(
    {},
    rendererBaseConfig(env),
    env.mode === "development"
      ? rendererDevConfig(env)
      : rendererProdConfig(env),
    new PresetManager().loadPresets(env)
  );
}
